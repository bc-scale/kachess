-- This file is part of kachess, and is released under the Creative Commons
-- Zero 1.0 dedication: https://creativecommons.org/publicdomain/zero/1.0/

-- try to keep Runtime in sync with
-- https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime
--
-- I would put this comment near Runtime itself, but the autoformatter will
-- erase it... https://github.com/dhall-lang/dhall-haskell/issues/145
let Map = ./Preludev20MapType.dhall

let SplitArguments = { Type = { splitOn : Text, source : Text }, default = {=} }

let Ref =
      < arnOfLambdaInStackByName : Text
      | ssm : Text
      | ssmSplitSplat : SplitArguments.Type
      | value : Text
      | terraformRef : Text
      | terraformRefSplitSplat : SplitArguments.Type
      >

let EnvironmentMap = Map Text Ref

let Runtime =
      < nodejs
      | nodejs4_3
      | nodejs6_10
      | nodejs8_10
      | nodejs10_x
      | nodejs12_x
      | nodejs14_x
      | nodejs16_x
      >

let VPC = { securityGroupIds : List Ref, subnetIds : List Ref }

let Function =
      { Type =
          { name : Text
          , environment : Optional EnvironmentMap
          , handler : { file : Text, method : Text }
          , logRetentionInDays : Optional Natural
          , logSubscriptionDestinationArn : Optional Ref
          , memorySizeMB : Natural
          , reservedConcurrency : Optional Natural
          , roleArn : Optional Ref
          , runtime : Runtime
          , timeoutSeconds : Natural
          , vpc : Optional VPC
          , externalLibraries : Optional (List Text)
          }
      , default =
        { environment = None EnvironmentMap
        , logRetentionInDays = None Natural
        , logSubscriptionDestinationArn = None Ref
        , memorySizeMB = 128
        , reservedConcurrency = None Natural
        , roleArn = None Ref
        , runtime = Runtime.nodejs
        , timeoutSeconds = 3
        , vpc = None VPC
        , externalLibraries = None (List Text)
        }
      }

let Stack =
      { Type =
          { bucketPrefix : Text
          , service : Text
          , functions : List Function.Type
          , terraformInitArgs : Optional (List Text)
          , terraformApplyArgs : Optional (List Text)
          }
      , default =
        { functions = [] : List Function.Type
        , terraformInitArgs = None (List Text)
        , terraformApplyArgs = None (List Text)
        }
      }

let arnOfLambdaInStack
    : Function.Type → Ref
    = λ(x : Function.Type) → Ref.arnOfLambdaInStackByName x.name

in  { EnvironmentMap
    , Function
    , Ref
    , Runtime
    , SplitArguments
    , Stack
    , arnOfLambdaInStack
    , ssm = Ref.ssm
    , ssmSplitSplat = Ref.ssmSplitSplat
    , terraformRef = Ref.terraformRef
    , terraformRefSplitSplat = Ref.terraformRefSplitSplat
    , value = Ref.value
    }
