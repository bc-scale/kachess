#!/usr/bin/env bash

# This is a bit of a trimmed down version of a pattern we're experimenting with
# internally at LXB. It's subject to (potentially wild) change, and doesn't
# pretend to be all-encompassing.

# exit if anything fails
set -e

WHEREAMI=$(cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)

if [ -z "${REPO_ROOT}" ]; then
	REPO_ROOT=$(git rev-parse --show-toplevel || echo $(pwd))
fi

APPLICATION_NAME="dhall"

export IMAGE_NAME="dhallhaskell/dhall"
export IMAGE_TAG="1.39.0"

docker_extra_args=( $DOCKER_EXTRA_ARGS )

# This will print nothing and move on if the image exists, and something like
# "Error: No such image: org/project:1.2.3" if it does not, which is a
# *much* cleaner error message than the "invalid reference format" you'd get at
# "docker run" time if referring to a non-existent image
docker inspect --type image ${IMAGE_NAME}:${IMAGE_TAG} > /dev/null

# don't use -it for this incantation, the TTY allocated is for some reason
# invalid and the tool complains "the input device is not a TTY", which is a
# notoriously vague docker error message with more advertised solutions on
# Github than there are docker users in the world, methinks
docker run \
	--user $(id -u):$(id -g) \
	--rm \
	-e AWS_REGION \
	-e AWS_ACCESS_KEY_ID \
	-e AWS_SECRET_ACCESS_KEY \
	-e AWS_SESSION_TOKEN \
	-e AWS_SECURITY_TOKEN \
	-e AWS_ACCOUNT_NICKNAME \
	-e XDG_CACHE_HOME=${REPO_ROOT}/.cache \
	-w ${REPO_ROOT} \
	-v ${REPO_ROOT}:/app \
	-v ${REPO_ROOT}:${REPO_ROOT} \
	-t "${docker_extra_args[@]}" \
	${IMAGE_NAME}:${IMAGE_TAG} \
	${APPLICATION_NAME} $@
