default:
  retry:
    max: 2
    when:
      # # Retry on any failure (default).
      # - always
      # Retry when the failure reason is unknown.
      - unknown_failure
      # Retry when the script failed.
      - script_failure
      # Retry on API failure.
      - api_failure
      # Retry when the job got stuck or timed out.
      - stuck_or_timeout_failure
      # Retry if there is a runner system failure (for example, job setup failed).
      - runner_system_failure
      # Retry if the runner is unsupported.
      - runner_unsupported
      # Retry if a delayed job could not be executed.
      - stale_schedule
      # Retry if the script exceeded the maximum execution time set for the job.
      - job_execution_timeout
      # # Retry if the job is archived and can't be run.
      # - archived_failure
      # # Retry if the job failed to complete prerequisite tasks.
      # - unmet_prerequisites
      # Retry if the scheduler failed to assign the job to a runner.
      - scheduler_failure
      # Retry if there is a structural integrity problem detected.
      - data_integrity_failure

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  TF_INPUT: "false"
  TF_IN_AUTOMATION: "true"

stages:
  - tag
  - build

.image-kaniko: &image-kaniko
  name: gcr.io/kaniko-project/executor:v1.8.1-debug
  entrypoint: [""]

# Credentials for all our registries.
.step-container-registry-creds: &step-container-registry-creds |-
  mkdir -p /kaniko/.docker || true

  tee /kaniko/.docker/config.json <<-EOF 2>&1 > /dev/null
  {
    "credHelpers": {
      "public.ecr.aws": "ecr-login",
      "${LXB_ECR}": "ecr-login"
    },
    "auths": {
      "${CI_REGISTRY}": {
        "auth": "$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')"
      },
      "${LXB_ECR}": {},
      "https://index.docker.io/v1/": {
        "auth": "$(printf "%s:%s" "${DOCKER_HUB_USER_LXB}" "${DOCKER_HUB_TOKEN_LXB}" | base64 | tr -d '\n')"
      },
      "https://index.docker.io/v1/": {
        "auth": "$(printf "%s:%s" "${DOCKER_HUB_USER_LXB}" "${DOCKER_HUB_TOKEN_LXB}" | base64 | tr -d '\n')"
      }
    }
  }
  EOF

build-docker-image:
  stage: build
  allow_failure: false
  timeout: 2 hours 0 minutes
  image: *image-kaniko
  variables:
    CI_IMAGE_NAME: "$CI_REGISTRY/logixboard/farmers-market/ci"
    CI_IMAGE_TAG: "git-$CI_COMMIT_SHORT_SHA"
    IMAGE_NAME: "$CI_IMAGE_NAME"
  script:
    - *step-container-registry-creds
    - echo $(/kaniko/executor version)
    # Cache 2190h0m0s = 3 months.
    - |-
      anotherTag="$(echo ${CI_COMMIT_TAG} | grep -qE '^[0-9]+' && echo 'latest' || echo 'latest-dev')"

        # --customPlatform linux/amd64,linux/arm64 \
      /kaniko/executor \
        --context . \
        --destination "${CI_REGISTRY_IMAGE}:git-${CI_COMMIT_SHORT_SHA}" \
        --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" \
        --destination "${CI_REGISTRY_IMAGE}:${anotherTag}" \
        --cache=true \
        --cache-repo "${CI_REGISTRY_IMAGE}" \
        --cache-ttl 2190h0m0s \
        --ignore-path=/root/.aws \
        --skip-unused-stages=true \
        --snapshotMode=redo \
        --use-new-run \
        --compressed-caching=false \
        --push-retry 3

tagging:
  stage: tag
  image: alpine/git:v2.34.2
  only:
    - main
  before_script:
    - |-
      echo "Setting git branch 'origin' to 'https://${CI_REGISTRY_USER}:${DEV_USER_GITLAB_TOKEN}@${CI_REPOSITORY_URL#*@}'..."
      git remote set-url origin https://${CI_REGISTRY_USER}:${DEV_USER_GITLAB_TOKEN}@${CI_REPOSITORY_URL#*@}
      echo 'Configuring the git user...'
      git config --global user.email "dev+gitlab@logixboard.com"
      git config --global user.name "LXB Automation"
  script: |-
    tag=$(cat version)
    git tag -f -a -m "${tag}" "${tag}"
    git push -f --tags origin "HEAD:${CI_COMMIT_BRANCH}"
