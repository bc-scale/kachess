# kachess: an opinionated deployment tool for AWS Lambda

kachess deploys JavaScript-based AWS Lambda stacks quickly and reliably, using [esbuild](https://esbuild.github.io/) and [Terraform](https://www.terraform.io/). It is particularly well-suited to, and intentionally designed for (however, not exclusively), monorepos. It aims to do only what it must, as correctly and efficiently as it can.

`kachess` targets Terraform 1.0 and esbuild 0.11. We plan to update this in the near future to some newer flavor of esbuild.

"kachess" is the name of [a lake in Washington State](https://en.wikipedia.org/wiki/Kachess_Lake), named after the local indigenous term for "more fish".

## Requirements

To run kachess, either:

- Use the provided Docker image `registry.gitlab.com/logixboard/kachess`. The `:latest` tag tracks the most recent Git tag, or you can lock to any Git tag or version number with `:vX.Y.Z`, or any Git sha with `:git-1234567` - see the full list [here](https://gitlab.com/logixboard/kachess/container_registry/2076443).

> The provided Docker image supports `linux/amd64` and `linux/arm64` platforms. By extension, this means that Docker for Mac on Apple Silicon should Just Work.

- Build from source using Rust 1.46 or newer. In the latter case, you'll need the following peer dependencies also installed on your system:

  - [esbuild](https://esbuild.github.io) (tested against v0.11.6)
  - [terraform](https://terraform.io) (tested against v1.0)

If using the Docker image, you'll want to pass any necessary environment variables through, and map the container's user ID to your host system so you can edit any files it writes. For example, here's (roughly) what we do at Logixboard:

```sh
#!/usr/bin/env bash

# put this in your $PATH as `kachess`, perhaps. if just copy-pasting into a
# shell, you may need to adapt some things

IMAGE_NAME="registry.gitlab.com/logixboard/kachess"
IMAGE_TAG="0.17.0"

if [ -z "${REPO_ROOT}" ]; then
  REPO_ROOT=$(git rev-parse --show-toplevel || echo $(pwd))
fi

docker run \
  --user $(id -u):$(id -g) \
  --rm \
  -e AWS_REGION \
  -e AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY \
  -e AWS_SESSION_TOKEN \
  -e AWS_SECURITY_TOKEN \
  -e AWS_ACCOUNT_NICKNAME \
  -v ${REPO_ROOT}:/app \
  -v ${REPO_ROOT}:${REPO_ROOT} \
  -it ${IMAGE_NAME}:${IMAGE_TAG} \
  kachess $@
```

## Usage

### CLI

```text
USAGE:
    kachess [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <FILE>              a kachess config file in dhall format [default: ./kachess.dhall]
    -o, --out-file <FILE.tf.json>    file to write Terraform JSON to (must end in .tf.json) [default: ./kachess-
                                     generated.tf.json]
    -p, --parallelism <INT>          number of esbuild jobs to run in parallel [default: 1]
```

For now, to destroy a stack made with Kachess, remove `kachess-generated.tf.json` and simply `terraform destroy`.

### Configuration

kachess is configured using [Dhall](dhall-lang.org/), a strongly-typed and
composable, if sometimes verbose, configuration format. See the [Dhall language
tour](https://docs.dhall-lang.org/tutorials/Language-Tour.html) and
`schema.dhall` in this repo to get an idea for how to actually stand up a
`kachess` stack. Actual examples are planned, TBA :)

## Developing Kachess

Ensure you have:

- [rustc and cargo](https://www.rust-lang.org/) 1.46+.
- [rustfmt](https://github.com/rust-lang/rustfmt).
- `pkg_config` and `openssl`, as build-time dependencies of `dhall_serde`.
- `dhall` and `dhall-json` are recommended for debugging schemas.

## Release

1. When `make lint-rs` succeeds on your local machine, push your commits to a branch, go through the MR process.

2. When a Logixboard employee feels the state of `main` warrants a version bump, they can bump `Cargo.toml` to reflect the new version number, run `cargo build` to ensure `Cargo.lock` is up to date, commit the result, and run `git tag -a X.Y.Z`, where `X.Y.Z` matches the version number in `Cargo.toml`. After providing a tag message, `git push && git push --tags` will push both the updated `main` and the version tag, which will trigger two CI jobs. When the tag's job succeeds, the new Kachess version will be available in the Docker registry.

## License

The code powering kachess is released under the ISC License, as follows:

> Copyright 2021-2022 Logixboard Inc.
>
> Permission to use, copy, modify, and/or distribute this software for any
> purpose with or without fee is hereby granted, provided that the above
> copyright notice and this permission notice appear in all copies.
>
> THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
> REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
> AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
> INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
> LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
> OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
> PERFORMANCE OF THIS SOFTWARE.

This README, the Dhall schema, and all other documentation are released under the
[Creative Commons CC0 1.0
Universal](https://creativecommons.org/publicdomain/zero/1.0/).

`Preludev20MapType.dhall` is copied from [Dhall's upstream
repo](https://github.com/dhall-lang/dhall-lang) and is thus licensed BSD-3, see
the file header for a copy of that license.
