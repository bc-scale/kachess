/* This file is part of kachess
 *
 * Copyright 2021-2022 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

use custom_error::custom_error;

custom_error! {
    pub BundlingError EsbuildExited = "esbuild exited unexpectedly",
}

custom_error! { pub TerraformGenerationError
    SSMSplitInapplicableInContext{parameter:String} = "attempted to split-and-splat an SSM parameter, {}, where a list is not applicable!",
    TFRefSplitInapplicableInContext{parameter:String} = "attempted to split-and-splat a TerraformRef, {}, where a list is not applicable!",
    LambdaARNUsedAsSGID{name:String} = "attempted to use ARN of Lambda in stack, named {}, as security group ID for a Lambda in stack!",
    FunctionNameTooLong{name:String, max_length:usize} ="function name {} would exceed AWS-defined maximum length of {} and be truncated, potentially ambiguously; refusing to continue",
    S3BucketNameTooLong{name:String, max_length:usize} ="S3 bucket name {} would exceed AWS-defined maximum length of {} and be truncated, potentially ambiguously; refusing to continue",
}

pub enum ExitReason {
    ErrInvalidCli(String),
    ErrRequisiteFsActions(String),
    ErrInvalidConfiguration(String),
    ErrEsbuildBundlingFailed,
    ErrGeneratedTerraformInvalid(String),
    ErrTerraformInitFailed,
    ErrTerraformApplyFailed,
}

impl From<ExitReason> for i32 {
    fn from(status: ExitReason) -> Self {
        match status {
            ExitReason::ErrInvalidCli(_) => 1,
            ExitReason::ErrRequisiteFsActions(_) => 2,
            ExitReason::ErrInvalidConfiguration(_) => 3,
            ExitReason::ErrEsbuildBundlingFailed => 4,
            ExitReason::ErrGeneratedTerraformInvalid(_) => 5,
            ExitReason::ErrTerraformInitFailed => 6,
            ExitReason::ErrTerraformApplyFailed => 7,
        }
    }
}
