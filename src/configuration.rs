/* This file is part of kachess
 *
 * Copyright 2021-2022 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

use super::memory_size_mb::MemorySizeMB;
use super::timeout_seconds::TimeoutSeconds;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::{Path, PathBuf};

/*****
 * IF YOU UPDATE THE STRUCTS IN THIS FILE, UPDATE THE SCHEMA IN schema.dhall
 */

pub type FunctionEnvironment = HashMap<String, ValueOrRef>;

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Configuration {
    pub bucket_prefix: String,
    pub service: String,
    pub functions: Vec<Function>,
    pub terraform_init_args: Option<Vec<String>>,
    pub terraform_apply_args: Option<Vec<String>>,
}
#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Function {
    pub name: String,
    pub handler: FunctionHandler,
    pub environment: Option<FunctionEnvironment>,
    pub external_libraries: Option<Vec<String>>,
    pub log_retention_in_days: Option<usize>,
    pub log_subscription_destination_arn: Option<ValueOrRef>,
    #[serde(default, rename = "memorySizeMB")]
    pub memory_size_mb: MemorySizeMB,
    #[serde(default)]
    pub timeout_seconds: TimeoutSeconds,
    pub reserved_concurrency: Option<usize>,
    pub runtime: Runtime,
    pub role_arn: Option<ValueOrRef>,
    pub vpc: Option<VPC>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct FunctionHandler {
    pub file: PathBuf,
    pub method: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
// try to keep in sync with
// https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime
pub enum Runtime {
    Nodejs,
    #[serde(rename = "nodejs4_3")]
    Nodejs43,
    #[serde(rename = "nodejs6_10")]
    Nodejs610,
    #[serde(rename = "nodejs8_10")]
    Nodejs810,
    Nodejs10X,
    Nodejs12X,
    Nodejs14X,
    Nodejs16X,
}

impl Runtime {
    pub fn to_esbuild_target_arg(&self) -> &str {
        match self {
            Runtime::Nodejs => &"esnext",
            Runtime::Nodejs43 => &"node43",
            Runtime::Nodejs610 => &"node610",
            Runtime::Nodejs810 => &"node810",
            Runtime::Nodejs10X => &"node10",
            Runtime::Nodejs12X => &"node12",
            Runtime::Nodejs14X => &"node14",
            Runtime::Nodejs16X => &"node16",
        }
    }

    pub fn to_terraform_string(&self) -> &str {
        match self {
            Runtime::Nodejs => "nodejs",
            Runtime::Nodejs43 => "nodejs4.3",
            Runtime::Nodejs610 => "nodejs6.10",
            Runtime::Nodejs810 => "nodejs8.10",
            Runtime::Nodejs10X => "nodejs10.x",
            Runtime::Nodejs12X => "nodejs12.x",
            Runtime::Nodejs14X => "nodejs14.x",
            Runtime::Nodejs16X => "nodejs16.x",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SplitArguments {
    pub split_on: String,
    pub source: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub enum ValueOrRef {
    #[serde(rename = "arnOfLambdaInStackByName")]
    ARNOfLambdaInStackByName(String),
    #[serde(rename = "ssm")]
    SSM(String),

    // okay, look, I don't love this either, but I have yet to figure out a particularly cleaner
    // way of expressing, in Dhall, that a value exists as a CSV in SSM somewhere. this is a
    // particularly useful pattern for cases like enumerating VPC subnets, where the length of the
    // list is sometimes not a detail a Lambda deployment should need to be familiar with. this
    // allows us to split those values and "splat" them into the list of subnetIds (or wherever
    // else this proves useful)
    #[serde(rename = "ssmSplitSplat")]
    SSMSplitSplat(SplitArguments),

    Value(String),
    TerraformRef(String),

    // see same note as with SSMSplitSplat
    TerraformRefSplitSplat(SplitArguments),
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct VPC {
    pub security_group_ids: Vec<ValueOrRef>,
    pub subnet_ids: Vec<ValueOrRef>,
}

pub async fn get_configuration(filename: &Path) -> Result<Configuration, serde_dhall::Error> {
    tokio::task::block_in_place(move || serde_dhall::from_file(filename).parse())
}
