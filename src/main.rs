/* This file is part of kachess
 *
 * Copyright 2021-2022 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#[macro_use]
extern crate clap;
extern crate custom_error;
extern crate futures;
extern crate indicatif;
extern crate num;
extern crate serde;
extern crate serde_dhall;
extern crate serde_json;
extern crate tempdir;
extern crate tokio;

mod bars;
mod bundle;
mod configuration;
mod errors;
mod memory_size_mb;
mod terraform;
mod timeout_seconds;

use bars::kachess_spinner;
use bundle::bundle_functions;
use clap::App;
use configuration::{get_configuration, Configuration};
use errors::ExitReason;
use std::boxed::Box;
use std::fs;
use std::io::{self, Write};
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use tempdir::TempDir;
use terraform::generate_terraform_config;
use tokio::process::Command;

const APPLICATION_NAME: &str = "kachess";

#[tokio::main]
async fn main() {
    // this makes use of the From<ExitReason> impl for i32 found in errors.rs, if the .into() looks
    // goofy
    //
    // avoid using _ or other catch-all variables in these match blocks to ensure all possible
    // error cases have an associated exit code
    std::process::exit(match run_kachess().await {
        Ok(_) => 0,
        Err(err) => match &err {
            ExitReason::ErrInvalidCli(msg)
            | ExitReason::ErrRequisiteFsActions(msg)
            | ExitReason::ErrInvalidConfiguration(msg)
            | ExitReason::ErrGeneratedTerraformInvalid(msg) => {
                eprintln!("{}", msg);
                err.into()
            }
            ExitReason::ErrEsbuildBundlingFailed
            | ExitReason::ErrTerraformInitFailed
            | ExitReason::ErrTerraformApplyFailed => err.into(),
        },
    })
}

async fn run_kachess() -> Result<(), ExitReason> {
    let yaml = load_yaml!("../cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let dry_run = matches.occurrences_of("dry_run") > 0;
    let tmp_dir = Arc::new(TempDir::new(APPLICATION_NAME).map_err(|_| {
        ExitReason::ErrRequisiteFsActions(
            "could not create temporary directory to store esbuild output".to_string(),
        )
    })?);
    let config_path = matches.value_of("config").ok_or_else(|| {
        ExitReason::ErrInvalidConfiguration(
            "internal error: no config file or default specified".to_string(),
        )
    })?;
    let config_path_canon = Arc::new(
        fs::canonicalize(config_path)
            .map_err(|_| {
                ExitReason::ErrRequisiteFsActions(
                    format!("could not canonicalize config file path - does {} exist? (see kachess --help for more options)", config_path)
                )
            })
            .and_then(|cpath| {
                // just try to stat the file to ensure it'll be readable, to avoid ugly errors from
                // dhall-land
                fs::metadata(&cpath).map_err(|_| {
                    ExitReason::ErrRequisiteFsActions(format!(
                        "internal error: could not stat specified config file: {}",
                        config_path
                    ))
                })?;

                Ok(cpath)
            })?,
    );

    let config = load_configuration(config_path_canon.as_path()).await?;
    run_bundling(
        config,
        tmp_dir.clone(),
        config_path_canon.clone(),
        value_t!(matches, "parallelism", usize).map_err(|_| {
            ExitReason::ErrInvalidCli("parallelism arg must be a number".to_string())
        })?,
    )
    .await?;

    // dry run doesn't call terraform but instead just proves that a bundle could have been
    // created: exit early
    if dry_run {
        return Ok(());
    }

    generate_terraform_file(
        config,
        tmp_dir.clone(),
        matches.value_of("out_file").ok_or_else(|| {
            ExitReason::ErrInvalidCli("out_file arg must be a string".to_string())
        })?,
    )
    .await?;
    terraform_init(config).await?;
    terraform_apply(config).await?;

    Ok(())
}

async fn load_configuration(config_path: &Path) -> Result<&'static mut Configuration, ExitReason> {
    // importantly, relative imports only work correctly if using `from_file`, so don't use
    // `from_str` here
    //
    // leak the box to intentionally get a static reference which will survive long enough for
    // tokio's spawned processes to use it without cloning functions[] and etc.
    let bar = kachess_spinner(1, "Loading configuration...");
    let config: &'static mut Configuration =
        Box::leak(Box::new(get_configuration(config_path).await.map_err(
            |err| ExitReason::ErrInvalidConfiguration(err.to_string()),
        )?));
    bar.finish();

    Ok(config)
}

async fn run_bundling(
    config: &'static Configuration,
    tmp_dir: Arc<TempDir>,
    config_path: Arc<PathBuf>,
    max_jobs: usize,
) -> Result<(), ExitReason> {
    let bar = kachess_spinner(config.functions.len(), "Bundling functions...");
    bundle_functions(config, max_jobs, tmp_dir, config_path, bar.clone())
        .await
        .map_err(|_| ExitReason::ErrEsbuildBundlingFailed)?;
    bar.finish();
    Ok(())
}

async fn generate_terraform_file(
    config: &'static Configuration,
    tmp_dir: Arc<TempDir>,
    target: &str,
) -> Result<(), ExitReason> {
    let tf_config = generate_terraform_config(&config, &tmp_dir)
        .map_err(|err| ExitReason::ErrGeneratedTerraformInvalid(err.to_string()))?;

    std::fs::write(
        target,
        serde_json::to_string_pretty(&tf_config).map_err(|_| {
            ExitReason::ErrGeneratedTerraformInvalid(
                "internal error: could not translate generated Terraform config to JSON"
                    .to_string(),
            )
        })?,
    )
    .map_err(|_| {
        ExitReason::ErrRequisiteFsActions(
            "could not write generated Terraform to specified out_file".to_string(),
        )
    })
}

async fn terraform_init(config: &'static Configuration) -> Result<(), ExitReason> {
    let bar = kachess_spinner(1, "Initializing Terraform...");
    let mut tf_init = Command::new("terraform");
    tf_init
        .arg("init")
        .args(
            config
                .terraform_init_args
                .clone()
                .unwrap_or_else(std::vec::Vec::new),
        )
        .env("KACHESS_BUILD", "1");
    let output = tf_init.output().await.expect("terraform init failed");
    if !output.status.success() {
        eprintln!("terraform init failed!");
        io::stderr()
            .write_all(&output.stderr)
            .expect("internal error: could not write errors to stderr");
        return Err(ExitReason::ErrTerraformInitFailed);
    }
    bar.finish();

    Ok(())
}

async fn terraform_apply(config: &'static Configuration) -> Result<(), ExitReason> {
    let bar = kachess_spinner(1, "Applying Terraform...");
    let mut tf_apply = Command::new("terraform");
    tf_apply
        .arg("apply")
        .args(
            config
                .terraform_apply_args
                .clone()
                .unwrap_or_else(std::vec::Vec::new),
        )
        .env("KACHESS_BUILD", "1");
    let output = tf_apply.status().await.expect("terraform apply failed");
    bar.finish();

    if output.success() {
        Ok(())
    } else {
        Err(ExitReason::ErrTerraformApplyFailed)
    }
}
