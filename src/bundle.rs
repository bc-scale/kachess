/* This file is part of kachess
 *
 * Copyright 2021-2022 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

use super::configuration::{Configuration, Function};
use super::errors::BundlingError;
use indicatif::ProgressBar;
use std::collections::HashMap;
use std::io::{self, Write};
use std::path::PathBuf;
use std::sync::Arc;
use tempdir::TempDir;
use tokio::process::Command;
use tokio::sync::{Mutex, Semaphore};

type ErrorsMap = Arc<Mutex<HashMap<String, Vec<u8>>>>;

pub async fn bundle_functions(
    config: &'static Configuration,
    max_jobs: usize,
    tmp_dir: Arc<TempDir>,
    config_path_canon: Arc<PathBuf>,
    bar: Arc<ProgressBar>,
) -> Result<(), BundlingError> {
    let permission = Arc::new(Semaphore::new(max_jobs));
    let stderrs: ErrorsMap = Arc::new(Mutex::new(HashMap::new()));
    let any_failed: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));

    let job_futures = config.functions.iter().map(|func| {
        let tmp_dir = tmp_dir.clone();
        let config_path_canon = config_path_canon.clone();
        let permission = permission.clone();
        let stderrs = stderrs.clone();
        let bar = bar.clone();
        let any_failed = any_failed.clone();

        tokio::spawn(async move {
            let mut job = create_bundle_job(&func, tmp_dir, config_path_canon);

            // per
            // https://www.reddit.com/r/rust/comments/jj7z7f/optimizing_away_unused_variables/gabpouo/,
            // we actually need to name this variable despite marking it unused, to prevent
            // destructors running immediately (thus freeing the semaphore lock). the more you
            // know!
            if let Ok(_permit) = permission.acquire().await {
                match job.output().await {
                    Ok(output) => {
                        if !output.stderr.is_empty() {
                            let mut errmap = stderrs.lock().await;
                            errmap.insert(func.name.clone(), output.stderr);
                        }

                        if !output.status.success() {
                            bar.finish();
                            eprintln!("failed to bundle function {}, exiting...", &func.name);
                            permission.close();
                            *any_failed.lock().await = true;
                        }
                    }
                    Err(err) => {
                        let mut errmap = stderrs.lock().await;
                        errmap.insert(func.name.clone(), format!("{:?}", err).into_bytes());
                        permission.close();
                        *any_failed.lock().await = true;
                    }
                }
            }

            // if the semaphore was already closed and thus failed to acquire, we don't actually
            // have to do anything special. just inc the bar and get outta here!

            bar.inc(1);
        })
    });

    futures::future::join_all(job_futures).await;

    let errmap = stderrs.lock().await;
    if errmap.len() > 0 {
        for (func_name, stderr) in errmap.iter() {
            eprintln!();
            eprintln!("Warnings or errors encountered bundling {}:", func_name);
            io::stderr()
                .write_all(stderr)
                .expect("could not write to stderr");
            eprintln!();
        }
    }

    if *any_failed.lock().await {
        Err(BundlingError::EsbuildExited)
    } else {
        Ok(())
    }
}

pub fn outfile_from_func(func: &Function, tmp_dir: &Arc<TempDir>) -> String {
    tmp_dir
        .path()
        .join(format!("{}.js", &func.name))
        .into_os_string()
        .into_string()
        .expect("internal error: generated an unusable output file string")
}

fn create_bundle_job(
    func: &Function,
    tmp_dir: Arc<TempDir>,
    config_path_canon: Arc<PathBuf>,
) -> Command {
    let outfile = outfile_from_func(func, &tmp_dir);

    // * operator derefs to the underlying object, which we can then clone into a local
    // mutable val. thanks, https://stackoverflow.com/a/55750742
    let mut entrypoint_path = (*config_path_canon).clone();
    entrypoint_path.pop();
    entrypoint_path.push(func.handler.file.clone());
    let entrypoint = entrypoint_path
        .into_os_string()
        .into_string()
        .expect("internal error: generated an unusable entrypoint file string");

    let mut job = Command::new("esbuild");
    job.args(&[
        "--log-level=warning", // quiet the stderr to just actual issues
        "--bundle",
        "--external:aws-sdk", // always provided by the runtime, save some space!
        &format!("--outfile={}", &outfile),
        "--platform=node", // for now, not configurable
        &esbuild_target_arg(&func),
        &entrypoint,
    ])
    .env("KACHESS_BUILD", "1");

    if let Some(externals) = &func.external_libraries {
        for ext in externals {
            job.arg(&format!("--external:{}", ext));
        }
    };

    job
}

fn esbuild_target_arg(func: &Function) -> String {
    format!("--target={}", func.runtime.to_esbuild_target_arg(),)
}
