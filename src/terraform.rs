/* This file is part of kachess
 *
 * Copyright 2021-2022 Logixboard Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without
 * fee is hereby granted, provided that the above copyright notice and this permission notice
 * appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

use super::bundle::outfile_from_func;
use super::configuration::{
    Configuration, Function, FunctionEnvironment, SplitArguments, ValueOrRef,
};
use super::errors::TerraformGenerationError;
use super::memory_size_mb::MemorySizeMB;
use super::timeout_seconds::TimeoutSeconds;
use aws_arn::ARN;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::Arc;
use tempdir::TempDir;

type TerraformResourceAWSLambdaFunctionsMap<'a> =
    HashMap<&'a str, TerraformResourceAWSLambdaFunction<'a>>;
type SSMReferences = HashMap<String, u8>;
type CloudwatchLogSubscriptionFilters<'a> =
    HashMap<&'a str, TerraformResourceAWSCloudwatchLogSubscriptionFilterInst<'a>>;
type LambdaPermissions<'a> = HashMap<String, TerraformResourceAWSLambdaPermissionInst<'a>>;
type CloudwatchLogGroups<'a> = HashMap<&'a str, TerraformResourceAWSCloudwatchLogGroupInst>;
type S3BucketObjects<'a> = HashMap<&'a str, TerraformResourceAWSS3BucketObjectInst<'a>>;

// inclusive maximum for each entity type
const ENTITY_NAME_LENGTH_MAX_S3_BUCKET: usize = 63;
const ENTITY_NAME_LENGTH_MAX_LAMBDA_FUNCTION: usize = 64;

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformLifecycle {
    pub create_before_destroy: bool,
    pub prevent_destroy: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfig<'a> {
    #[serde(borrow)]
    pub data: TerraformConfigData<'a>,
    #[serde(borrow)]
    pub locals: TerraformConfigLocals<'a>,
    #[serde(borrow)]
    pub resource: TerraformConfigResource<'a>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigLocals<'a> {
    pub _kachess_all_referenced_ssm_paths: SSMReferences,
    #[serde(borrow)]
    pub _kachess_func_names_to_sources: HashMap<&'a str, String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigData<'a> {
    pub archive_file: TerraformConfigDataArchiveFile,
    pub aws_ssm_parameter: TerraformConfigDataAWSSSMParameter,
    #[serde(borrow)]
    pub aws_iam_policy_document: TerraformConfigAWSIAMPolicyDocument<'a>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigDataArchiveFile {
    pub _kachess_lambda_zipped: TerraformConfigDataArchiveFileInst,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigDataAWSSSMParameter {
    kachess_ssm_lookup: TerraformConfigDataAWSSSMParameterInst,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigDataAWSSSMParameterInst {
    for_each: String,
    name: String,
    with_decryption: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigDataArchiveFileInst {
    pub for_each: String,
    pub output_path: String,
    pub source_file: String,
    #[serde(rename = "type")]
    pub archive_type: TerraformArchiveType,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigAWSIAMPolicyDocument<'a> {
    #[serde(borrow)]
    kachess_log_groups: TerraformConfigAWSIAMPolicyDocumentInst<'a>,
    #[serde(borrow)]
    kachess_role_assume: TerraformConfigAWSIAMPolicyDocumentInst<'a>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigAWSIAMPolicyDocumentInst<'a> {
    #[serde(borrow)]
    pub statement: TerraformConfigAWSIAMPolicyDocumentInstStatement<'a>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigAWSIAMPolicyDocumentInstStatement<'a> {
    pub effect: &'a str,
    pub actions: Vec<&'a str>,
    #[serde(borrow)]
    pub resources: Option<Vec<&'a str>>,
    #[serde(borrow)]
    pub principals: Option<TerraformConfigAWSIAMPolicyDocumentInstStatementPrincipals<'a>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigAWSIAMPolicyDocumentInstStatementPrincipals<'a> {
    #[serde(rename = "type", borrow)]
    principal_type: &'a str,
    identifiers: Vec<&'a str>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum TerraformArchiveType {
    Zip,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigResource<'a> {
    pub aws_iam_role: TerraformConfigResourceAWSIAMRole,
    pub aws_iam_role_policy: TerraformConfigResourceAWSIAMRolePolicy,
    #[serde(borrow)]
    pub aws_lambda_function: TerraformResourceAWSLambdaFunctionsMap<'a>,
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub aws_lambda_permission: HashMap<String, TerraformResourceAWSLambdaPermissionInst<'a>>,
    #[serde(borrow)]
    pub aws_s3_bucket: TerraformResourceAWSS3Bucket<'a>,
    #[serde(borrow)]
    pub aws_s3_bucket_object: HashMap<&'a str, TerraformResourceAWSS3BucketObjectInst<'a>>,
    #[serde(borrow)]
    pub aws_cloudwatch_log_group: HashMap<&'a str, TerraformResourceAWSCloudwatchLogGroupInst>,
    #[serde(borrow, skip_serializing_if = "HashMap::is_empty")]
    pub aws_cloudwatch_log_subscription_filter:
        HashMap<&'a str, TerraformResourceAWSCloudwatchLogSubscriptionFilterInst<'a>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigResourceAWSIAMRolePolicy {
    kachess_log_groups: TerraformConfigResourceAWSIAMRolePolicyInst,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigResourceAWSIAMRolePolicyInst {
    role: String,
    policy: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSS3Bucket<'a> {
    #[serde(borrow)]
    kachess_functions: TerraformResourceAWSS3BucketInst<'a>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSS3BucketInst<'a> {
    bucket: String,
    #[serde(borrow)]
    acl: &'a str,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSS3BucketObjectInst<'a> {
    pub bucket: String,
    pub etag: String,
    pub key: String,
    pub source: String,
    pub server_side_encryption: &'a str,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSCloudwatchLogGroupInst {
    pub name: String,
    pub retention_in_days: usize,
    pub lifecycle: TerraformLifecycle,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSCloudwatchLogSubscriptionFilterInst<'a> {
    pub name: String,
    pub destination_arn: String,
    pub filter_pattern: &'a str,
    pub log_group_name: String,

    // the subscription will have to depend on both the log group and the lambda, so not optional
    pub depends_on: Vec<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSLambdaPermissionInst<'a> {
    pub action: &'a str,
    pub function_name: String,
    pub principal: &'a str,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSLambdaFunction<'a> {
    pub function_name: String,
    pub handler: String,
    pub s3_bucket: String,
    pub s3_key: String,
    pub source_code_hash: String,
    pub runtime: &'a str,
    pub memory_size: MemorySizeMB,
    pub timeout: TimeoutSeconds,
    pub reserved_concurrent_executions: Option<usize>,
    pub role: String,
    pub environment: Option<TerraformResourceAWSLambdaFunctionEnvironment>,
    pub vpc_config: Option<TerraformResourceAWSLambdaFunctionVPC>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSLambdaFunctionEnvironment {
    pub variables: HashMap<String, String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformResourceAWSLambdaFunctionVPC {
    pub security_group_ids: String,
    pub subnet_ids: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigResourceAWSIAMRole {
    pub kachess_default: TerraformConfigResourceAWSIAMRoleInst,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct TerraformConfigResourceAWSIAMRoleInst {
    pub assume_role_policy: String,
    pub name: String,
}

pub fn generate_terraform_config<'a>(
    config: &'a Configuration,
    tmp_dir: &'a Arc<TempDir>,
) -> Result<TerraformConfig<'a>, TerraformGenerationError> {
    let all_ssm_paths = all_referenced_ssm_paths(config);

    let mut func_names_to_sources: HashMap<&str, String> = HashMap::new();
    let mut s3_bucket_objects: S3BucketObjects = HashMap::new();
    let mut cloudwatch_log_groups: CloudwatchLogGroups = HashMap::new();
    let mut cloudwatch_log_subscription_filters: CloudwatchLogSubscriptionFilters = HashMap::new();
    let mut lambda_permissions: LambdaPermissions = HashMap::new();

    let bucket_name = format!("{}_kchs-fns_{}", config.bucket_prefix, config.service);

    if bucket_name.len() > ENTITY_NAME_LENGTH_MAX_S3_BUCKET {
        return Err(TerraformGenerationError::S3BucketNameTooLong {
            name: bucket_name,
            max_length: ENTITY_NAME_LENGTH_MAX_S3_BUCKET,
        });
    }

    let bucket_name = tfref_lookup_str(&format!("replace(\"{}\", \"_\", \"-\")", bucket_name));

    for func in config.functions.iter() {
        if func.name.len() > ENTITY_NAME_LENGTH_MAX_LAMBDA_FUNCTION {
            return Err(TerraformGenerationError::FunctionNameTooLong {
                name: func.name.clone(),
                max_length: ENTITY_NAME_LENGTH_MAX_LAMBDA_FUNCTION,
            });
        }

        func_names_to_sources.insert(&func.name, outfile_from_func(&func, &tmp_dir));

        s3_bucket_objects.insert(
            &func.name,
            TerraformResourceAWSS3BucketObjectInst {
                bucket: tfref_lookup_str("aws_s3_bucket.kachess_functions.id"),
                etag: tfref_lookup_str(&format!(
                    "filemd5(data.archive_file._kachess_lambda_zipped[\"{}\"].output_path)",
                    &func.name
                )),
                key: format!("{}.js", &func.name),
                server_side_encryption: "AES256",
                source: tfref_lookup_str(&format!(
                    "data.archive_file._kachess_lambda_zipped[\"{}\"].output_path",
                    &func.name
                )),
            },
        );

        cloudwatch_log_groups.insert(
            &func.name,
            TerraformResourceAWSCloudwatchLogGroupInst {
                name: format!(
                    "/aws/lambda/{}",
                    tfref_lookup_str(&format!("aws_lambda_function.{}.function_name", &func.name)),
                ),
                retention_in_days: func.log_retention_in_days.unwrap_or(0),
                lifecycle: TerraformLifecycle {
                    create_before_destroy: true,
                    prevent_destroy: false,
                },
            },
        );

        generate_log_subscriptions_for_func(
            &func,
            &config,
            &mut cloudwatch_log_subscription_filters,
            &mut cloudwatch_log_groups,
            &mut lambda_permissions,
        )?;
    }

    Ok(TerraformConfig {
        locals: TerraformConfigLocals {
            _kachess_all_referenced_ssm_paths: all_ssm_paths,
            _kachess_func_names_to_sources: func_names_to_sources,
        },
        data: TerraformConfigData {
            archive_file: TerraformConfigDataArchiveFile {
                _kachess_lambda_zipped: TerraformConfigDataArchiveFileInst {
                    for_each: tfref_lookup_str("local._kachess_func_names_to_sources"),
                    output_path: format!(
                        "{}/{}.bundle.zip",
                        tmp_dir.path().to_str().unwrap(),
                        tfref_lookup_str("each.key"),
                    ),
                    source_file: tfref_lookup_str("each.value"),
                    archive_type: TerraformArchiveType::Zip,
                },
            },
            aws_iam_policy_document: TerraformConfigAWSIAMPolicyDocument {
                kachess_log_groups: TerraformConfigAWSIAMPolicyDocumentInst {
                    statement: TerraformConfigAWSIAMPolicyDocumentInstStatement {
                        effect: "Allow",
                        actions: vec!["logs:CreateLogStream", "logs:PutLogEvents"],
                        resources: Some(vec!["arn:aws:logs:*:*:*"]),
                        principals: None,
                    },
                },
                kachess_role_assume: TerraformConfigAWSIAMPolicyDocumentInst {
                    statement: TerraformConfigAWSIAMPolicyDocumentInstStatement {
                        effect: "Allow",
                        actions: vec!["sts:AssumeRole"],
                        resources: None,
                        principals: Some(
                            TerraformConfigAWSIAMPolicyDocumentInstStatementPrincipals {
                                principal_type: "Service",
                                identifiers: vec!["lambda.amazonaws.com"],
                            },
                        ),
                    },
                },
            },
            aws_ssm_parameter: TerraformConfigDataAWSSSMParameter {
                kachess_ssm_lookup: TerraformConfigDataAWSSSMParameterInst {
                    for_each: tfref_lookup_str("local._kachess_all_referenced_ssm_paths"),
                    name: tfref_lookup_str("each.key"),
                    with_decryption: true,
                },
            },
        },
        resource: TerraformConfigResource {
            aws_iam_role: TerraformConfigResourceAWSIAMRole {
                kachess_default: TerraformConfigResourceAWSIAMRoleInst {
                    name: format!("kachess_{}_default", config.service),
                    assume_role_policy: tfref_lookup_str(
                        "data.aws_iam_policy_document.kachess_role_assume.json",
                    ),
                },
            },
            aws_iam_role_policy: TerraformConfigResourceAWSIAMRolePolicy {
                kachess_log_groups: TerraformConfigResourceAWSIAMRolePolicyInst {
                    role: tfref_lookup_str("aws_iam_role.kachess_default.id"),
                    policy: tfref_lookup_str(
                        "data.aws_iam_policy_document.kachess_log_groups.json",
                    ),
                },
            },
            aws_lambda_function: functions_to_terraform(config)?,
            aws_lambda_permission: lambda_permissions,
            aws_s3_bucket: TerraformResourceAWSS3Bucket {
                kachess_functions: TerraformResourceAWSS3BucketInst {
                    bucket: bucket_name,
                    acl: "private",
                },
            },
            aws_s3_bucket_object: s3_bucket_objects,
            aws_cloudwatch_log_group: cloudwatch_log_groups,
            aws_cloudwatch_log_subscription_filter: cloudwatch_log_subscription_filters,
        },
    })
}

fn all_referenced_ssm_paths(config: &Configuration) -> SSMReferences {
    // using a HashMap here instead of a HashSet because Terraform will need a map type to look up
    // by names instead of indicies anyway
    let mut all_ssm_paths: SSMReferences = HashMap::new();
    for func in config.functions.iter() {
        if let Some(ValueOrRef::SSM(path)) = &func.role_arn {
            all_ssm_paths.insert(path.clone(), 1);
        }

        if let Some(ValueOrRef::SSM(path)) = &func.log_subscription_destination_arn {
            all_ssm_paths.insert(path.clone(), 1);
        }

        if let Some(env) = &func.environment {
            for (_, val) in env.iter() {
                if let ValueOrRef::SSM(path) = val {
                    all_ssm_paths.insert(path.clone(), 1);
                }
                if let ValueOrRef::SSMSplitSplat(args) = val {
                    all_ssm_paths.insert(args.source.clone(), 1);
                }
            }
        }

        if let Some(vpc) = &func.vpc {
            for sg in &vpc.security_group_ids {
                if let ValueOrRef::SSM(path) = sg {
                    all_ssm_paths.insert(path.clone(), 1);
                }
                if let ValueOrRef::SSMSplitSplat(args) = sg {
                    all_ssm_paths.insert(args.source.clone(), 1);
                }
            }
            for sn in &vpc.subnet_ids {
                if let ValueOrRef::SSM(path) = sn {
                    all_ssm_paths.insert(path.clone(), 1);
                }
                if let ValueOrRef::SSMSplitSplat(args) = sn {
                    all_ssm_paths.insert(args.source.clone(), 1);
                }
            }
        }
    }

    all_ssm_paths
}

fn arn_by_lambda_name_lookup_str(fname: &str) -> String {
    tfref_lookup_str(&format!("aws_lambda_function.{}.arn", fname))
}

fn ssm_param_lookup_str(ssm_path: &str) -> String {
    tfref_lookup_str(&ssm_param_lookup_tfref(ssm_path))
}

fn ssm_param_lookup_tfref(ssm_path: &str) -> String {
    format!(
        "data.aws_ssm_parameter.kachess_ssm_lookup[\"{}\"].value",
        ssm_path
    )
}

fn tfref_lookup_str(tfref: &str) -> String {
    format!("${{{}}}", tfref)
}

fn functions_to_terraform(
    config: &Configuration,
) -> Result<TerraformResourceAWSLambdaFunctionsMap, TerraformGenerationError> {
    let mut functions: TerraformResourceAWSLambdaFunctionsMap = HashMap::new();

    for func in config.functions.iter() {
        functions.insert(
            &func.name,
            TerraformResourceAWSLambdaFunction {
                function_name: format!("kchs_{}_{}", &config.service, &func.name),
                handler: format!(
                    "{}.{}",
                    func.handler.file.file_stem().unwrap().to_str().unwrap(),
                    func.handler.method.clone(),
                ),
                s3_bucket: tfref_lookup_str("aws_s3_bucket.kachess_functions.id"),
                s3_key: tfref_lookup_str(&format!("aws_s3_bucket_object.{}.key", &func.name)),
                source_code_hash: tfref_lookup_str(&format!(
                    "data.archive_file._kachess_lambda_zipped[\"{}\"].output_base64sha256",
                    &func.name
                )),
                runtime: func.runtime.to_terraform_string(),
                memory_size: func.memory_size_mb,
                timeout: func.timeout_seconds,
                reserved_concurrent_executions: func.reserved_concurrency,
                role: translate_function_role(&func)?,
                environment: translate_function_environment(&func)?,
                vpc_config: translate_function_vpc(&func)?,
            },
        );
    }

    Ok(functions)
}

fn env_from_function(
    env: &FunctionEnvironment,
) -> Result<HashMap<String, String>, TerraformGenerationError> {
    let mut ret: HashMap<String, String> = HashMap::new();
    for (key, val) in env.iter() {
        ret.insert(
            key.clone(),
            match val {
                ValueOrRef::ARNOfLambdaInStackByName(name) => {
                    Ok(arn_by_lambda_name_lookup_str(name).to_string())
                }
                ValueOrRef::SSM(key) => Ok(ssm_param_lookup_str(key).to_string()),
                ValueOrRef::SSMSplitSplat(args) => {
                    Err(TerraformGenerationError::SSMSplitInapplicableInContext {
                        parameter: args.source.clone(),
                    })
                }
                ValueOrRef::TerraformRef(tfref) => Ok(tfref_lookup_str(tfref).to_string()),
                ValueOrRef::TerraformRefSplitSplat(args) => {
                    Err(TerraformGenerationError::TFRefSplitInapplicableInContext {
                        parameter: args.source.clone(),
                    })
                }
                ValueOrRef::Value(val) => Ok(val.clone()),
            }?,
        );
    }

    Ok(ret)
}

fn translate_vec_valueorref_to_tf(coll: &[ValueOrRef]) -> Result<String, TerraformGenerationError> {
    let mut ret: Vec<String> = Vec::new();
    let mut splats: Vec<Box<SplitArguments>> = Vec::new();

    for it in coll {
        match it {
            ValueOrRef::ARNOfLambdaInStackByName(name) => {
                Err(TerraformGenerationError::LambdaARNUsedAsSGID { name: name.clone() })
            }
            ValueOrRef::SSM(key) => {
                ret.push(ssm_param_lookup_str(key).to_string());
                Ok(())
            }
            ValueOrRef::SSMSplitSplat(args) => {
                splats.push(Box::new(SplitArguments {
                    split_on: args.split_on.clone(),
                    source: ssm_param_lookup_tfref(&args.source).to_string(),
                }));
                Ok(())
            }
            ValueOrRef::TerraformRef(tfref) => {
                ret.push(tfref_lookup_str(tfref).to_string());
                Ok(())
            }
            ValueOrRef::TerraformRefSplitSplat(args) => {
                splats.push(Box::new(args.clone()));
                Ok(())
            }
            ValueOrRef::Value(val) => {
                ret.push(val.clone());
                Ok(())
            }
        }?
    }

    let splats: Vec<String> = splats
        .iter()
        .map(|split_args| format!("split(\"{}\", {})", split_args.split_on, split_args.source))
        .collect();

    // forgive me, for I have committed crimes against... everything, tbh
    Ok(tfref_lookup_str(&format!(
        "concat([{}{}{}], {})",
        if !ret.is_empty() { "\"" } else { "" },
        ret.join("\",\""),
        if !ret.is_empty() { "\"" } else { "" },
        if !splats.is_empty() {
            splats.join(",")
        } else {
            "[]".to_string()
        },
    )))
}

fn generate_log_subscriptions_for_func<'a>(
    func: &'a Function,
    config: &Configuration,
    filters: &mut CloudwatchLogSubscriptionFilters<'a>,
    log_groups: &mut CloudwatchLogGroups,
    lambda_permissions: &mut LambdaPermissions,
) -> Result<(), TerraformGenerationError> {
    if func.log_subscription_destination_arn.is_none() {
        return Ok(());
    }

    let destination_arn = match func.log_subscription_destination_arn.as_ref().unwrap() {
        ValueOrRef::ARNOfLambdaInStackByName(name) => {
            let ret = arn_by_lambda_name_lookup_str(&name);

            lambda_permissions.insert(
                func.name.clone(),
                TerraformResourceAWSLambdaPermissionInst {
                    action: "lambda:InvokeFunction",
                    function_name: tfref_lookup_str(&format!(
                        "aws_lambda_function.{}.function_name",
                        &func.name
                    )),
                    principal: "logs.amazonaws.com",
                },
            );

            Ok(ret)
        }
        ValueOrRef::SSMSplitSplat(args) => {
            Err(TerraformGenerationError::SSMSplitInapplicableInContext {
                parameter: args.source.clone(),
            })
        }
        ValueOrRef::SSM(key) => Ok(ssm_param_lookup_str(&key)),
        ValueOrRef::TerraformRef(tfref) => Ok(tfref_lookup_str(&tfref)),
        ValueOrRef::TerraformRefSplitSplat(args) => {
            Err(TerraformGenerationError::TFRefSplitInapplicableInContext {
                parameter: args.source.clone(),
            })
        }
        ValueOrRef::Value(val) => {
            let ret = val.clone();

            let arn: ARN = ret
                .parse()
                .expect("provided value doesn't look like an ARN");

            if arn.service == aws_arn::known::Service::Lambda.into() {
                lambda_permissions.insert(
                    format!("{}", arn.resource),
                    TerraformResourceAWSLambdaPermissionInst {
                        action: "lambda:InvokeFunction",
                        function_name: tfref_lookup_str(&format!(
                            "aws_lambda_function.{}.function_name",
                            &func.name
                        )),
                        principal: "logs.amazonaws.com",
                    },
                );
            }

            Ok(ret)
        }
    }?;

    let filter = TerraformResourceAWSCloudwatchLogSubscriptionFilterInst {
        name: format!("kachess-log-filter-{}-{}", &config.service, &func.name),
        filter_pattern: "",
        // this works around the fact that strings deref to &str but we need a real String
        // https://stackoverflow.com/a/65550108
        log_group_name: log_groups.get(&*func.name).unwrap().name.clone(),
        destination_arn,
        depends_on: vec![
            format!("aws_lambda_function.{}", &func.name),
            format!("aws_cloudwatch_log_group.{}", &func.name),
        ],
    };

    filters.insert(&func.name, filter);

    Ok(())
}

fn translate_function_role(func: &Function) -> Result<String, TerraformGenerationError> {
    match &func.role_arn {
        None => Ok(tfref_lookup_str("aws_iam_role.kachess_default.arn")),
        Some(ValueOrRef::ARNOfLambdaInStackByName(name)) => {
            Err(TerraformGenerationError::LambdaARNUsedAsSGID { name: name.clone() })
        }
        Some(ValueOrRef::SSMSplitSplat(args)) => {
            Err(TerraformGenerationError::SSMSplitInapplicableInContext {
                parameter: args.source.clone(),
            })
        }
        Some(ValueOrRef::SSM(key)) => Ok(ssm_param_lookup_str(key)),
        Some(ValueOrRef::TerraformRef(tfref)) => Ok(tfref_lookup_str(tfref)),
        Some(ValueOrRef::TerraformRefSplitSplat(args)) => {
            Err(TerraformGenerationError::TFRefSplitInapplicableInContext {
                parameter: args.source.clone(),
            })
        }
        Some(ValueOrRef::Value(val)) => Ok(val.clone()),
    }
}

fn translate_function_environment(
    func: &Function,
) -> Result<Option<TerraformResourceAWSLambdaFunctionEnvironment>, TerraformGenerationError> {
    match &func.environment {
        None => Ok(None),
        Some(env) => env_from_function(&env)
            .map(|vars| Some(TerraformResourceAWSLambdaFunctionEnvironment { variables: vars })),
    }
}

fn translate_function_vpc(
    func: &Function,
) -> Result<Option<TerraformResourceAWSLambdaFunctionVPC>, TerraformGenerationError> {
    match &func.vpc {
        None => Ok(None),
        Some(vpc) => Ok(Some(TerraformResourceAWSLambdaFunctionVPC {
            security_group_ids: translate_vec_valueorref_to_tf(&vpc.security_group_ids)?,
            subnet_ids: translate_vec_valueorref_to_tf(&vpc.subnet_ids)?,
        })),
    }
}
