FROM public.ecr.aws/docker/library/golang:1.16-alpine3.14 AS esbuild-builder

WORKDIR /go/src/app

RUN apk add --no-cache --update \
	alpine-sdk \
	git

RUN git clone --depth 1 --branch v0.11.6 https://github.com/evanw/esbuild.git

WORKDIR /go/src/app/esbuild

RUN go build  --ldflags '-linkmode external -extldflags "-static"' ./cmd/esbuild

CMD ["./esbuild"]

FROM public.ecr.aws/docker/library/alpine:3.14 AS terraform-builder

RUN apk add --no-cache --update \
	curl \
	unzip

RUN ARCH=$([ $(uname -m) == 'x86_64' ] && echo "amd64" || echo "arm64") \
	sh -c 'curl -LO https://releases.hashicorp.com/terraform/1.2.1/terraform_1.2.1_linux_$ARCH.zip && unzip terraform_1.2.1_linux_$ARCH.zip'

RUN mv terraform /bin/

CMD ["/bin/terraform"]

FROM public.ecr.aws/docker/library/rust:1.54.0-alpine AS kachess-builder
WORKDIR /usr/src/

# since Alpine dynamically links their musl, we'll just go with the flow and
# not attempt to statically link *anything*
#
# https://users.rust-lang.org/t/sigsegv-with-program-linked-against-openssl-in-an-alpine-container/52172/4
ENV RUSTFLAGS="-C target-feature=-crt-static"

RUN apk add --no-cache --update \
	alpine-sdk \
	ca-certificates \
	openssl-dev

# RUN rustup update stable
RUN rustup default nightly && rustup update

COPY ./Cargo.toml ./
COPY ./Cargo.lock ./
COPY ./cli.yml ./
COPY ./src/ ./src/
RUN cargo install --path .

FROM public.ecr.aws/docker/library/alpine:3.14 AS release

# git gets pulled in because Terraform supports git::https protocol
RUN apk add --no-cache --update \
	ca-certificates \
	libgcc \
	libssl1.1 \
	git

COPY --from=esbuild-builder /go/src/app/esbuild/esbuild /bin/esbuild
COPY --from=terraform-builder /bin/terraform /bin/terraform
COPY --from=kachess-builder /usr/local/cargo/bin/kachess /bin/kachess

# this is usable with something like the following, presuming you've set
# KACHESS_IMAGE and KACHESS_VERSION to something reasonable:
#
# docker run \
# 	-e AWS_REGION \
# 	-e AWS_ACCESS_KEY_ID \
# 	-e AWS_SECRET_ACCESS_KEY \
# 	-e AWS_SESSION_TOKEN \
# 	-e AWS_SECURITY_TOKEN \
# 	--rm \
# 	-v $(pwd):/app \
# 	-it ${KACHESS_IMAGE}:${KACHESS_VERSION}
CMD ["kachess"]
